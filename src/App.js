import React, { Component } from "react"
import { NavbarComponent, Header, FooterComponent } from "./components"
import { Tentang, Kelebihan, Foto, CaraMemesan, Kontak } from "./pages"
import { Container } from "react-bootstrap"

export default class App extends Component {
  render() {
    return (
      <div>
        {/* navbar */}
        <NavbarComponent />

        {/* Header */}
        <Header />

        {/* Halaman Kelebihan & Foto */}
        <Container>
          <Tentang />
          <Kelebihan />
          <Foto />
          <CaraMemesan />
          <Kontak />
        </Container>

        {/* footer */}
        <FooterComponent />
      </div >
    );
  }
}
