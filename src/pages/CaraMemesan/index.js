import React, { Component } from 'react'

export default class CaraMemesan extends Component {
    render() {
        return (
            <div id="cara-memesan">
                <h1 className="heading-title" data-aos="fade-down">Cara <span className="nikutsu-text">memesan</span></h1>
                <div className="mobile d-sm-block d-md-none" data-aos="fade-down">
                    <p className="text-paragraf-2">
                        Jika ingin memesan produk kami silahkan kunjungi toko online kami di shopee & tokopedia (nikutsu official)
                        atau bisa klik tombol dibawah ini.
                    </p>
                </div>

                <div className="desktop d-none d-md-block" data-aos="fade-down">
                    <p className="text-paragraf-2">
                        Jika ingin memesan produk kami silahkan kunjungi toko online kami di shopee & tokopedia (nikutsu official)
                        <br />
                        atau bisa klik tombol dibawah ini.
                    </p>
                </div>
                <div data-aos="fade-down">
                    <a target="_blank" rel="noreferrer" href="https://shopee.co.id/shop/594863981/" className="btn btn-shopee">Shopee</a>
                    <a target="_blank" rel="noreferrer" href="https://tokopedia.com" className="btn btn-tokopedia">Tokopedia</a>
                </div>
            </div>
        )
    }
}
