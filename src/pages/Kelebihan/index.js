import React, { Component } from 'react'
import { CardComponent } from '../../components'
import { dummyKelebihans } from '../../data'

export default class Kelebihan extends Component {
    constructor(props) {
        super(props)

        this.state = {
            kelebihans: dummyKelebihans
        }
    }
    render() {
        const { kelebihans } = this.state
        return (
            <div>
                <h1 className="heading-title" data-aos="fade-down">Kelebihan berbelanja di <span className="nikutsu-text">nikutsu</span></h1>
                <CardComponent kelebihans={kelebihans} />
            </div>
        )
    }
}
