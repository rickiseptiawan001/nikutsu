import React, { Component } from "react"
import { CardProduct } from "../../components"
import { dummyProducts } from '../../data'

export default class Kelebihan extends Component {
    constructor(props) {
        super(props)

        this.state = {
            products: dummyProducts
        }
    }
    render() {
        const { products } = this.state
        return (
            <div>
                <h1 className="heading-title" data-aos="fade-down">
                    Foto sepatu <span className="nikutsu-text">nikutsu</span>
                </h1>
                <p className="text-paragraf-2" data-aos="fade-down">
                    Dibawah ini merupakan beberapa foto sepatu yang dijual di toko (nikutsu official), selengkapnya silahkan bisa lihat di shopee & tokopedia.
                </p>
                <CardProduct products={products} />
            </div>
        );
    }
}
