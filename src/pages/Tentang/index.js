import React, { Component } from 'react'

export default class Tentang extends Component {
    render() {
        return (
            <div id="tentang">
                <h1 className="heading-title" data-aos="fade-down">Tentang <span className="nikutsu-text">nikutsu</span></h1>
                <p className="text-paragraf" data-aos="fade-down">
                    Apa itu nikutsu ? nikutsu adalah toko yang menjual berbagai macam sepatu berkualitas dengan harga yang
                    terjangkau, pendiri toko nikutsu adalah (Abdul Muis), nikutsu tersedia di marketplace shopee & tokopedia, tokonya yang bernama
                    (nikutsu official).
                </p>
            </div>
        )
    }
}
