import Tentang from './Tentang'
import Kelebihan from './Kelebihan'
import Foto from './Foto'
import CaraMemesan from './CaraMemesan'
import Kontak from './Kontak'

export { Tentang, Kelebihan, Foto, CaraMemesan, Kontak }