import React, { Component } from 'react'

export default class Kontak extends Component {
    render() {
        return (
            <div id="kontak">
                <h1 className="heading-title" data-aos="fade-down">Kontak <span className="nikutsu-text">nikutsu</span></h1>

                <div className="mobile d-sm-block d-md-none" data-aos="fade-down">
                    <p className="text-paragraf-2">
                        Jika ingin menghubungi kami untuk menanyakan seputar produk yang kami jual
                        silahkan hubungi nomor di bawah ini.
                    </p>
                </div>

                <div className="desktop d-none d-md-block" data-aos="fade-down">
                    <p className="text-paragraf-2">
                        Jika ingin menghubungi kami untuk menanyakan seputar produk yang kami jual
                        <br />
                        silahkan hubungi nomor di bawah ini.
                    </p>
                </div>

                <a href="https://api.whatsapp.com/send?phone=62895615717791&text=Saya%20ingin%20bertanya%20seputar%20sepatu" className="btn btn-wa" data-aos="fade-down">
                    WhatsApp
                </a>
            </div>
        )
    }
}
