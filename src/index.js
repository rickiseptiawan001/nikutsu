import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

// import ini agar library bootstrap bisa dipakai
import 'bootstrap/dist/css/bootstrap.min.css'

// my styling
import '../src/assets/style/main.css'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
