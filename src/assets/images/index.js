import HeaderImage from './bg-header.png'
import IcWhatsApp from './ic-wa.png'
import Ilustrasi1 from './ilustrasi-1.svg'
import Ilustrasi2 from './ilustrasi-2.svg'
import Ilustrasi3 from './ilustrasi-3.svg'
import Logo from './logo.png'
import Fire from './fire.svg'

export { HeaderImage, IcWhatsApp, Ilustrasi1, Ilustrasi2, Ilustrasi3, Logo, Fire }