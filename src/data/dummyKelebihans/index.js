import { Ilustrasi1, Ilustrasi2, Ilustrasi3 } from '../../assets'

export const dummyKelebihans = [
    {
        id: 1,
        text: 'Harga Terjangkau',
        gambar: Ilustrasi1
    },
    {
        id: 2,
        text: 'Pengiriman yang cepat',
        gambar: Ilustrasi2
    },
    {
        id: 3,
        text: 'Kualitas Bagus',
        gambar: Ilustrasi3
    }
]