import { Product1, Product2, Product3, Product4 } from '../../assets'

export const dummyProducts = [
    {
        id: 1,
        gambar: Product1
    },
    {
        id: 2,
        gambar: Product2
    },
    {
        id: 3,
        gambar: Product3
    },
    {
        id: 4,
        gambar: Product4
    }
]