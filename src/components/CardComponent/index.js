import React from 'react'
import { Card, Row, Col } from 'react-bootstrap'

const CardComponent = ({ kelebihans }) => {
    return (
        <div data-aos="fade-down">
            <Row className="row-card">
                {kelebihans.map((kelebihan) => {
                    return (
                        <Col lg={4} key={kelebihan.id}>
                            <Card className="card-style">
                                <img src={kelebihan.gambar} className="ilustrasi-image" alt="gambar-ilustrasi" />
                            </Card>
                            <div className="footer-card">
                                <p className="pragraf-footer-card">{kelebihan.text}</p>
                            </div>
                        </Col>
                    )
                })}
            </Row>
        </div>
    )
}

export default CardComponent
