import React from "react"
import { Fire } from '../../assets';

const FooterComponent = () => {
    let tahunSekarang = new Date().getFullYear();
    return (
        <div>
            <footer className="footer">
                <p className="text-footer">Copyright {tahunSekarang} | nikutsu dibuat dengan semangat api
                    <img src={Fire} className="fire" alt="fire-icon" />
                </p>
            </footer>
        </div>
    );
}

export default FooterComponent
