import React, { Component } from 'react'
import { Nav, Navbar, Container } from 'react-bootstrap'
import { Logo } from '../../assets'

export default class NavbarComponent extends Component {
    render() {
        return (
            <div>
                <Navbar bg="dark" expand="lg" variant="dark">
                    <Container>
                        <Navbar.Brand href="#home">
                            <img src={Logo} className="logo-website" alt="logo" />
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ms-auto">
                                <Nav.Link href="/" className="navbar-menu">Home</Nav.Link>
                                <Nav.Link href="/#tentang" className="navbar-menu">Tentang</Nav.Link>
                                <Nav.Link href="/#cara-memesan" className="navbar-menu">Cara memesan</Nav.Link>
                                <Nav.Link href="/#kontak" className="navbar-menu">Kontak</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            </div>
        )
    }
}
