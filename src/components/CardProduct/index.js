import React from "react"
import { Card, Row, Col } from "react-bootstrap"

const CardProduct = ({ products }) => {
    return (
        <div data-aos="fade-down">
            <Row className="row-card">
                {products.map((product) => {
                    return (
                        <Col lg={6} key={product.id}>
                            <Card className="card-product-style">
                                <img src={product.gambar} className="product-image" alt="gambar-product" />
                            </Card>
                        </Col>
                    )
                })}
            </Row>
        </div>
    );
}

export default CardProduct
