import React, { Component } from 'react'
import { HeaderImage } from '../../assets'

export default class Header extends Component {
    render() {
        return (
            <div>
                <img src={HeaderImage} alt="Header" className="header-image" />
            </div>
        )
    }
}
